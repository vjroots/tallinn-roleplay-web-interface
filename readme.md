# Tallinn RolePlay Web Interface

Tallinn RolePlay web interface helps to manage GTA V RP community by adding admin features, police and ems records and lot more

## How to set up

- Clone this repository to your server

    ``
    git clone git@bitbucket.org:aivarsomelar/tallinn-roleplay-web-interface.git
    ``
    
- install dependencies with composer

    ``
    composer install
    ``
    
- Set up env variables. Composer install should crete a file named .env. If it does 
not create a file automatically, then make copy of .env.example file, rename it to .env and 
change variables inside a file

    ```
    ## Name of your page
    APP_NAME=Laravel
    ## name of env, if it's live server use "production"
    APP_ENV=local
    APP_KEY=
    ## set this "false" if it's live server
    APP_DEBUG=true
    APP_URL=http://localhost
    
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=homestead
    DB_USERNAME=homestead
    DB_PASSWORD=secret
    
    ## If you don't have mail server, leave next fields like they are. Email is used to
    ## send password reset link
    MAIL_DRIVER=smtp
    MAIL_HOST=smtp.mailtrap.io
    MAIL_PORT=2525
    MAIL_USERNAME=null
    MAIL_PASSWORD=null
    MAIL_ENCRYPTION=null
    ```
    App key can be generate by running command
    ``
    php artisan key:generate
    ``
    
- Run database migrations. This web server comes with additional DB tables and modifies 
on at least one ESX table. SO MAKE SURE YOU HAVE BACKUP OF YOUR ORIGINAL DATABASE.
    
    ``
    php artisan migrate
    ``
    
- If you are in localhost, to start server, run ``php artisan serve``. In live server you should
 not need that
 
### Discord link
- Open resources/views/welcome.blade.php. In line 21 and 22, change values according to your need.

## Updates

To update server run ``git pull`` to download newest updates. Always run ``composer install`` and 
``php artistan migrate``, because updates can introduce new dependencies and additional tables or 
table updates

## Contribution
You are free to change any of that code for your needs and I welcome pull request, but when you do that 
please fork this project.

Uncertainly I don't remember what plugin is used there and where to get invite code. 

### Author
Aivar Somelar <aivarsomelar@gmail.com>
