<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMedicalHistoryMedicId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('medical_histories', function (Blueprint $table) {
            $table->dropColumn('medicId');
            $table->integer('ems_worker_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::table('medical_histories', function (Blueprint $table) {
            $table->dropColumn('ems_worker_id');
            $table->string('medicId');
        });
    }
}
