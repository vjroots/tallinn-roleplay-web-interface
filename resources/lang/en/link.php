<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Links
    |--------------------------------------------------------------------------
    |
    | This file contains translations of links
    |
    */

    'home' => 'Home',
    'help' => 'Help',
    'job_applications' => 'Job Applications',
    'accepted_job_applications' => 'Accepted Job Applications',
    'police_application' => 'Police application',
    'ambulance_application' => 'Ambulance application',
    'police' => 'Police',
    'players_list' => 'Players List',
    'cars_list' => 'Cars List',
    'reports_list' => 'Reports List',
    'fines_list' => 'Fines List',
    'officers_list' => 'Officers List',
    'ambulance' => 'Ambulance',
    'workers_list' => 'Workers List',
    'car_dealer' => 'Car Dealer',
    'used_cars_sell_request_list' => 'Used Cars Sell Request List',
    'used_cars_sell_history' => 'Used Cars Sell History',
    'all_available_vehicles_marks' => 'All Available Vehicles Mark',
    'all_owned_cars' => 'All Owned Cars',
    'admin' => 'Admin',
    'all_users' => 'All Users',
    'whitelist_applications' => 'Whitelist Applications',
    'ck_candidates' => 'CK Candidates',
    'logout' => 'Logout',
    'login' => 'Login',
    'whitelist_application' => 'Whitelist Application',
    'my_data' => 'My Data',
    'my_car' => 'My Car',
    'criminal_records' => 'Criminal Records',
    'medical_records' => 'Medical Records',
    'my_Job_applications' => 'My Job Applications',
    'user_data' => 'User Data',
    'user_cars' => 'User Cars',
    'forgot_your_password' => 'Forgot Your Password?',
    'server_rules' => 'Server Rules',
    'police_rules' => 'Police Rules',
    'ems_rules' => 'Ambulance Rules',
    'mechanic' => 'Mechanic',
    'new_report' => 'New Report',
    'my_reports' => 'My Reports',
    'all_mechanic_reports' => 'All Reports',
    'radio' => 'Radio'
];
