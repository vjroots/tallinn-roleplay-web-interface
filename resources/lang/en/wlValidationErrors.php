<?php

return [
    'name_required' => 'Nimi on kohustuslik',
    'name_min' => 'Nimi peab olema vähemalt 2 tähemärki',
    'name_max' => 'Nimi ei tohi olla pikem kui 50 tähemärki',
    'character_required' => 'Karakteri nimi on kohustuslik',
    'character_min' => 'Karakteri nimi peab olema vähemalt 2 tähemärki',
    'character_max' => 'Karkteri nimi ei tohi olla pikem kui 50 tähemärki',
    'steam_required' => 'Steami nimi on kohustuslik',
    'steam_min' => 'Steami nimi peab olema vähemalt 2 tähemärki',
    'steam_max' => 'Steami nimi ei tohi olla pikem kui 20 tähemärki',
    'already_whitelisted' => 'Sa oled juba whitelist-is',
    'character_name_exists' => 'Selline karakteri nimi juba eksisteerib',
];
