<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons
    |--------------------------------------------------------------------------
    |
    | This file contains translations of buttons
    |
    */
    'user_data' => 'User Data',
    'bills' => 'Bills',
    'licenses' => 'Licenses',
    'properties' => 'Properties',
    'criminal_records' => 'Criminal Records',
    'characters' => 'Characters',
    'user_cars' => 'User Cars',
    'medical_history' => 'Medical History',
    'ems' => 'Ambulance',
    'police' => 'Police',
    'application' => 'Application',
    'police_job_application' => 'Police job application',
    'ems_job_application' => 'Ems job application',
    'fines' => 'Fines',
    'application_data' => 'Application data',
    'actions' => 'Actions',
    'car_sale_confirmation' => 'Car Sale confirmation',
    'login' => 'Login',
    'reset_password' => 'Reset Password',
    'whitelist_application_with_web_user_registration' => 'Whitelist Application With Web user registration',
    'fill_criminal_record_for' => 'Fill Criminal Record for',
    'money' => 'Money',
    'update_profile' => 'Update Profile',
    'car_data' => 'Car Data',
    'help' => 'Web page legend',
    'whitelist_application_info' => 'Whitelist application info',
    'password_reset' => 'Password Reset',
    'mechanic_new_report' => 'New Report',
    'mechanic_my_report' => 'My Report',
];
