<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Registration Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'picture_upload_failed' => 'Pildi ülesse laadimine ebaõnnestus. Palun proovi uuesti!',
    'picture_upload_success' => 'Pilt edukalt ülesse laetud',
    'wanted_character' => 'Isik on tagaotistav',

];
