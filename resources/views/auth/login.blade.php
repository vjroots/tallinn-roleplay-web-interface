@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card shadow-lg mb-3 mr-3">
                    <div class="card-header">{{ __('headers.login') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}" id="login">
                            @csrf

                            <div class="form-group row">
                                <label for="email"
                                       class="col-md-4 col-form-label text-md-right">{{ __('forms.e_mail_address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email"
                                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email" value="{{ old('email') }}" required autofocus
                                           >

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password"
                                       class="col-md-4 col-form-label text-md-right">{{ __('forms.password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password"
                                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                           name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="remember"
                                               id="remember" {{ old('remember') ? 'checked' : '' }}>

                                        <label class="form-check-label" for="remember">
                                            {{ __('forms.remember_me') }}
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary" id="confirmButton" disabled
                                            onclick="overlayOn()">
                                        {{ __('headers.login') }}
                                    </button>

                                    @if (Route::has('password.request'))
                                        <a class="btn btn-link" href="{{ route('password.request') }}"
                                           onclick="overlayOn()">
                                            {{ __('link.forgot_your_password') }}
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script>
    window.onload = function () {
        const emailField = document.getElementById('email');
        const passwordField = document.getElementById('password');
        const confirmButton = document.getElementById('confirmButton');
        const form = document.getElementById('login');

        let elementArray = [
            emailField,
            passwordField
        ];

        elementArray.forEach(function (element) {
            element.addEventListener('keyup', function (event) {
                let isValidEmailField = emailField.checkValidity();
                let isValidPasswordField = passwordField.checkValidity();

                if (
                    isValidEmailField
                    && isValidPasswordField
                ) {
                    confirmButton.disabled = false;
                } else {
                    confirmButton.disabled = true;
                }
            });
        });

        if (event.which == 13 || event.keyCode == 13) {
            if (!confirmButton.disabled) {
                overlayOn();
                form.submit();
            }
            return false;
        } else {
            return true;
        }
    };
</script>
