<div class="card-header mouse-over" data-toggle="collapse"
     href="#licensesBody"
     aria-expanded="false"
     aria-controls="licensesBody">
    <b>{{__('headers.licenses')}}</b>
    <span class="badge badge-pill badge-secondary">{{\App\UserLicenses::where('owner', $identifier)->get()->count()}}</span>
</div>
<div class="card-body collapse" id="licensesBody">
    <ul>
        @foreach($licenses as $license)
            @switch($license->type)
                @case('dmv')
                <li>{{__('texts.theory_exam')}}</li>
                @break
                @case('drive')
                <li>{{__('texts.car_driver_license')}}</li>
                @break
                @case('drive_bike')
                <li>{{__('texts.bike_driver_license')}}</li>
                @break
                @case('drive_truck')
                <li>{{__('texts.truck_driver_license')}}</li>
                @break
                @case('weapon')
                <li>{{__('texts.concealed_weapon_carry_license')}}</li>
                @break
                @case('drive_bus')
                <li>{{__('texts.bus_driver_license')}}</li>
                @break
            @endswitch
        @endforeach
    </ul>
</div>
