@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row mt-5 justify-content-center">
        <div class="title m-b-md">
            <img src="logo/tallinnrp2-w.png" class="img-fluid">
        </div>
    </div>
    <div class="row align-items-center d-flex text-center mt-5 justify-content-center">
            <div id="discordInviteBox"></div>
    </div>
</div>
    @component('components.spinner')
    @endcomponent
@endsection
