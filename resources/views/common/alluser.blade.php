@extends('layouts.app')

@section('content')
    <div class="container">
        @component(
            'components.searchBar',
             [
                'routeName' => $searchRoute,
                'placeholder' => $searchPlaceholder,
                'oldSearch' => (isset($oldSearch)) ? $oldSearch : null,
                'autocompleteUrl' => (isset($autocompleteUrl)) ? $autocompleteUrl : 'userSearchAutocomplete'
             ]
         )
        @endcomponent
        <div class="card">
            @component('components.usersTable', ['routeName' => $singleUserRoute, 'users' => $users, 'wanted' => (isset($wanted)) ? $wanted : null])
            @endcomponent
            @if(method_exists($users, 'links'))
                <div class="container">
                    <div class="pagination justify-content-center p-4">
                        {{$users->links()}}
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection
