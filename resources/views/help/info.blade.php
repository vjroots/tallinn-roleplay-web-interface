@extends('help.layout.layout')
@section('helpHeader')
    <strong>Üldine info</strong>
@endsection
@section('helpBody')
    <div>
        <h4><strong>Emoted</strong></h4>
        <div class="row">
            <div class="col-md-4">
                <ul>
                    <li>Example command 1</li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <li>Example command 2</li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <li>Example command 3</li>
                </ul>
            </div>
        </div>
    </div>
@endsection
