@if(isset($collapse) and $collapse)
    <div class="card-header mouse-over" data-toggle="collapse"
         href="#carsBody"
         aria-expanded="false"
         aria-controls="carsBody">
@else
<div class="card-header">
@endif
    <b>{{__('headers.user_cars')}}</b>
    <span class="badge badge-pill badge-secondary">{{\App\OwnedCar::where('owner', $owner->identifier)->get()->count()}}</span></div>
@if(isset($collapse) and $collapse)
    <div class="card-body collapse" id="carsBody">
@else
    <div class="card-body" id="carsBody">
@endif
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">{{__('tables.number_plate')}}</th>
            <th scope="col">{{__('tables.model_name')}}</th>
            <th scope="col">{{__('tables.is_insured')}}</th>
            <th scope="col">{{__('tables.insurance_end_date')}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($cars as $car)
            <tr  class="mouse-over" onclick="
                window.location='{{route('policeShowSingleCar', [$car->plate])}}';
                overlayOn();
                ">
                <td>{{$car->plate}}</td>
                <td>
                    @isset(\App\CarModel::where('code', json_decode($car->vehicle)->model)->first()->name)
                        {{\App\CarModel::where('code', json_decode($car->vehicle)->model)->first()->name}}
                    @endisset
                </td>
                <td>
                    @if(
                        isset(\App\CarInsurance::where('plate', $car->plate)->first()->is_insured)
                        and isset(\App\CarInsurance::where('plate', $car->plate)->first()->end_time)
                        and \App\CarInsurance::where('plate', $car->plate)->first()->is_insured
                        and new DateTime(\App\CarInsurance::where('plate', $car->plate)->first()->end_time) > new DateTime()
                    )
                        <div class="text-success">Yes</div>
                    @else
                        <div class="text-danger">No</div>
                    @endif
                </td>
                <td>
                    @if(isset(\App\CarInsurance::where('plate', $car->plate)->first()->end_time))
                        {{\App\CarInsurance::where('plate', $car->plate)->first()->end_time}}
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

</div>
