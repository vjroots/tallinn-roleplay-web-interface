@extends('layouts.app') @section('content')
<div class="container">
	@component('components.searchBar', ['routeName' => 'policeSearchUser',
	'placeholder' => __('forms.search_user')]) @endcomponent
	@component('police.components.secondaryNav', ['identifier' =>
	$user->identifier, 'active' => 'cars']) @endcomponent

	<div class="row">
		<div class="col-md-12">
			@if (session('status'))
			<div class="alert alert-success" role="alert">{{ session('status') }}
			</div>
			@endif

			<div class="card mt-5">
			@component('police.components.userCars',
				['cars' => $cars, 'owner' => $user]) @endcomponent
			</div>
		</div>
	</div>
</div>
@endsection

<script>

    /**
     *
     * @returns {boolean}
     * @constructor
     */
    function ConfirmDelete() {
        var x = confirm("Are you sure you want to continue?");
        if (x)
            return true;
        else
            return false;
    }

</script>
