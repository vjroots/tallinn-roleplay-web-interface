<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo as BelongsTo;

class CriminalRecord extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'criminal_records';

    /**
     * @return BelongsTo
     */
    public function user()
    {

        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function policeOfficer()
    {

        return $this->belongsTo(PoliceOfficer::class);
    }
}
