<?php

namespace App\Http\Controllers;

use App\Bill;
use App\Character;
use App\ConfiscateCarLog;
use App\CriminalRecord;
use App\FineType;
use App\Helpers\Sql\FivemFineTypes;
use App\Helpers\Sql\FivemUserHelper;
use App\Helpers\UserHelper;
use App\Helpers\WantedHelper;
use App\JobApplication;
use App\JobGrades;
use App\Motel;
use App\OwnedCar;
use App\OwnedMotel;
use App\OwnedProperty;
use App\PoliceOfficer;
use App\Property;
use App\StolenCar;
use App\User;
use App\UserLicenses;
use App\WantedCar;
use App\WantedCharacters;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

/**
 * Class AdminController
 */
class PoliceController extends Controller
{

    const ALLOWED_JOBS = [
        'police',
        'fib',
        'offpolice',
        'abipolitsei'
    ];
    const ALLOWED_JOB_GRADES = [
        5,
        6
    ];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('auth');
    }

    /**
     * Show list of all fivem users
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function allUsers()
    {

        $this->restrictAccess();

        return view(
            'common.alluser',
            [
                'users' => User::paginate(10),
                'wanted' => WantedHelper::getAllWantedCharacters(),
                'searchRoute' => 'policeSearchUser',
                'searchPlaceholder' => __('forms.search_user'),
                'singleUserRoute' => 'policeSingleUser',
                'autocompleteUrl' => 'userSearchAutocomplete'
            ]
        );
    }

    /**
     * Show single fivem user info and actions
     *
     * @param Request $request
     * @param $steamId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function singleUser($steamId)
    {

        $this->restrictAccess();

        $user = User::find($steamId);

        $ownedProperties = [];
        foreach (OwnedProperty::where('owner', $steamId)->get() as $ownedProperty) {
            $property = new \stdClass();
            $property->name = $ownedProperty->name;
            $property->isRented = $ownedProperty->rented;
            $property->label = Property::where('name', $ownedProperty->name)->first()->label;
            $ownedProperties[] = $property;
        }

        foreach (OwnedMotel::where('owner', $steamId)->get() as $ownedMotel) {
            $property = new \stdClass();
            $property->name = $ownedMotel->name;
            $property->isRented = true;
            $property->label = Motel::where('name', $ownedMotel->name)->first()->label;
            $ownedProperties[] = $property;
        }

        return view(
            'police.user',
            [
                'user' => $user,
                'bills' => Bill::where('identifier', $steamId)->get(),
                'criminalRecords' => $user->criminalRecords()->orderBy('created_at', 'DESC')->get(),
                'characters' => Character::where('identifier', $steamId)->get(),
                'wanted' => $user->wantedCharacter()->get(),
                'licenses' => UserLicenses::where('owner', $steamId)->get(),
                'ownedProperties' => $ownedProperties,
                'searchPlaceholder' => __('forms.search_user'),
                'profilePicture' => ($user->webUser()->first()->profilePicture()->orderBy('id', 'DESC')->first()) ? $user->webUser()->first()->profilePicture()->orderBy('id', 'DESC')->first()->path : null
            ]
        );
    }
    /**
     * Show single fivem user info and actions
     *
     * @param Request $request
     * @param $steamId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function singleUserCars($steamId)
    {

        $this->restrictAccess();

        return view(
            'police.cars',
            [
                'user' => User::find($steamId),
                'cars' => FivemUserHelper::getUserCars($steamId)
            ]
        );
    }

    /**
     * Show search results of users
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function searchUser(Request $request)
    {

        $this->restrictAccess();
        return view(
            'common.alluser',
            [
                'users' => FivemUserHelper::searchUsers($request->search),
                'wanted' => WantedHelper::getAllWantedCharacters(),
                'searchRoute' => 'policeSearchUser',
                'searchPlaceholder' => 'search user',
                'singleUserRoute' => 'policeSingleUser',
                'oldSearch' => $request->search
            ]
        );
    }

    /**
     * @param Request $request
     * @param $plate
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showSingleCar($plate)
    {

        $this->restrictAccess();
        $car = OwnedCar::where('plate', $plate)->first();

        return view(
            'common.carData',
            [
                'car' => $car,
                'user' => User::find($car->owner),
                'wantedCar' => WantedCar::where('plate', $car->plate)->where('is_wanted', true)->first(),
                'stolenCar' => StolenCar::where('plate', $car->plate)->where('is_stolen', true)->first()
            ]
        );
    }

    /**
     * Delete user car
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function deleteCar(Request $request)
    {
        $this->restrictAccess();

        $loggedInUserData = Auth::user()->fiveMUser()->first();

        $owner = User::find($request->get('carOwner'));

        $log = new ConfiscateCarLog();
        $log->confiscator = $loggedInUserData->identifier;
        $log->confiscatorName = $loggedInUserData->name;
        $log->carOwner = $owner->identifier;
        $log->carOwnerName = $owner->name;
        $log->carPlate = $request->get('carPlate');
        $log->save();

        OwnedCar::find($request->carPlate)->delete();

        return redirect(route('policeSingleUser', ['steamId' => $request->carOwner]));
    }

    /**
     * Show report creation form
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function createCriminalRecord(Request $request)
    {

        $this->restrictAccess();

        $bills = Bill::where('identifier', $request->criminalIdentifier)->get();

        try {
            $minJail = FivemFineTypes::getMinJailTime($bills);
            $maxJail = FivemFineTypes::getMaxJailTime($bills);
        } catch (\Exception $exception) {
            $minJail = 0;
            $maxJail = 0;
        }

        return view(
            'police.criminalRecordForm',
            [
                'criminalIdentifier' => $request->get('criminalIdentifier'),
                'officerIdentifier' => $request->get('officerIdentifier'),
                'bills' => $bills,
                'minJail' => $minJail,
                'maxJail' => $maxJail

            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function addCriminalRecord(Request $request)
    {

        $this->restrictAccess();
        Validator::make(
            $request->all(),
            [
                'summary' => 'required|string|max:5000'
            ]
        )->validate();

        $fines = [];
        foreach (Bill::where('identifier', $request->criminalId)->get() as $fine) {
            if ($fine->target === 'society_police') {
                $thatFine = [
                    'label' => $fine->label,
                    'amount' => $fine->amount
                ];
                $fines[] = $thatFine;
            }
        }

        $criminalRecordModel = new CriminalRecord();
        $criminalRecordModel->user_identifier = $request->criminalId;
        $criminalRecordModel->character_name = UserHelper::getCharacterName(User::find($request->criminalId));
        $criminalRecordModel->offence_type = 'puudub';
        $criminalRecordModel->fine = json_encode($fines);
        $criminalRecordModel->jail_time = $request->jailTime;
        $criminalRecordModel->warning_car = ($request->warningCar) ? true : false;
        $criminalRecordModel->warning_driver_license = ($request->warningDriverLicense) ? true : false;
        $criminalRecordModel->warning_gun_license = ($request->warningGunLicense) ? true : false;
        $criminalRecordModel->summary = $request->summary;
        $criminalRecordModel->police_officer_id = Auth::user()->getFiveMUSerData()->policeOfficer()->first()->id;
        $criminalRecordModel->is_criminal_case = ($request->criminalCase) ? true : false;
        if ($request->criminalCase) {
            $criminalRecordModel->criminal_case_expires_at = (new \DateTime())->modify('+1 week');
        }
        $criminalRecordModel->save();

        return redirect(route('policeSingleUser', ['steamId' => $request->criminalId]));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function deleteCriminalRecord(Request $request)
    {

        $this->restrictAccess();
        $record = CriminalRecord::find($request->criminalRecordId);

        $recordOwner = $record->user_identifier;
        if (
            Auth::user()->getFiveMUserData()->policeOfficer->first()->id !== $record->police_officer_id and
            !in_array(Auth::user()->fiveMUser()->first()->job_grade, self::ALLOWED_JOB_GRADES) and
            !Auth::user()->hasRole('admin')
        ) {
            return redirect()->back()->withErrors(['You have insufficient permissions to do that action']);
        }

        $record->delete();

        return redirect(route('policeSingleUser', ['steamId' => $recordOwner]));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function deleteFine(Request $request)
    {

        $this->restrictAccess();

        Bill::find($request->billId)->delete();

        return redirect(route('policeSingleUser', ['steamId' => $request->billOwner]));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showCriminalRecord($id)
    {

        $this->restrictAccess();
        return view('police.criminalRecord', ['criminalRecord' => CriminalRecord::with(['user'])->find($id)]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function declareAsFugitive(Request $request)
    {

        request()->validate([

            'picture' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'reason' => 'required'

        ]);
        $this->restrictAccess();
        $loggedInUserData = Auth::user()->fiveMUser()->first();

        $wanted = new WantedCharacters();
        $wanted->user_identifier = $request->userSteamId;
        $wanted->character_name = $request->characterName;
        $wanted->is_wanted = true;
        $wanted->reason = $request->reason;
        $wanted->creator_id = $loggedInUserData->identifier;
        $wanted->creator_name = $loggedInUserData->firstname . ' ' . $loggedInUserData->lastname;

        if($request->picture) {
            $imageName = time() . '.' . $request->picture->getClientOriginalExtension();
            $request->picture->move(public_path('images'), $imageName);

            $wanted->image = $imageName;
        }

        $wanted->save();

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function unDeclareAsFugitive(Request $request)
    {

        $this->restrictAccess();

        $wanted = WantedCharacters::find($request->wantedId);
        $filename = $wanted->image;
        if($filename){
            File::delete('images/' . $filename);
        }
        $wanted->delete();

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showAllReports( )
    {

        $this->restrictAccess();
        return view(
            'police.allRecords',
            [
                'records' => CriminalRecord::with(['user', 'policeOfficer'])
                    ->orderBy('created_at', 'desc')
                    ->paginate(10)
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function searchReports(Request $request)
    {

        $this->restrictAccess();
        $records = FivemUserHelper::searchRecords($request->search);

        return view(
            'police.allRecords',
            [
                'records' => $records,
                'oldSearch' => $request->search
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showAllCars()
    {

        $this->restrictAccess();
        $cars = OwnedCar::paginate(10);

        return view(
            'common.allCars',
            [
                'cars' => $cars,
                'route' => 'policeShowSingleCar',
                'autocompleteUrl' => 'carPlateSearchAutocomplete',
                'searchRoute' => 'searchCar'
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function searchCar(Request $request)
    {

        $this->restrictAccess();;
        $cars = DB::table('owned_vehicles')
            ->whereRaw('LOWER(plate) like :plate', ['plate' => '%' . strtolower($request->input('search')) . '%'])
            ->get();

        return view(
            'common.allCars',
            [
                'cars' => $cars,
                'route' => 'policeShowSingleCar',
                'oldSearch' => $request->search,
                'autocompleteUrl' => 'carPlateSearchAutocomplete',
                'searchRoute' => 'searchCar'
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function declareCarAsWanted(Request $request)
    {

        $this->restrictAccess();

        $wantedCar = WantedCar::where('plate', $request->plate)->first();
        $loggedInUserData = Auth::user()->getFiveMUserData();

        if(!$wantedCar) {
            $wantedCar = new WantedCar();
            $wantedCar->plate = $request->plate;
            $wantedCar->reason = $request->reason;
            $wantedCar->is_wanted = true;
            $wantedCar->police_id = $loggedInUserData->identifier;
            $wantedCar->officer_name = $loggedInUserData->firstname . ' ' . $loggedInUserData->lastname;
            if($request->image) {
                $imageName = time() . '.' . $request->image->getClientOriginalExtension();
                $request->image->move(public_path('images'), $imageName);

                $wantedCar->image = $imageName;
            }

            $wantedCar->save();

            return redirect()->back()->with('status', 'Car has been declared as wanted');
        }

        $wantedCar->is_wanted = true;
        $wantedCar->reason = $request->reason;
        $wantedCar->police_id = $loggedInUserData->identifier;
        $wantedCar->officer_name = $loggedInUserData->firstname . ' ' . $loggedInUserData->lastname;

        if($request->image) {
            $imageName = time() . '.' . $request->image->getClientOriginalExtension();
            $request->image->move(public_path('images'), $imageName);

            $wantedCar->image = $imageName;
        }

        $wantedCar->save();

        return redirect()->back()->with('status', 'Car has been declared as wanted');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function unDeclareCarAsWanted(Request $request)
    {

        $this->restrictAccess();

        $wantedCar = WantedCar::where('plate', $request->plate)->first();

        if (!$wantedCar) {
            return redirect()->back()->with('error', 'Car with that plate was not found in wanted cars list');
        }

        $filename = $wantedCar->image;
        if($filename){
            File::delete('images/' . $filename);
        }
        $wantedCar->delete();

        return redirect()->back()->with('status', 'Car removed from wanted list');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function declareCarAsStolen(Request $request)
    {

        $this->restrictAccess();

        $stolenCar = StolenCar::where('plate', $request->plate)->first();

        if (!$stolenCar) {

            $stolenCar = new StolenCar();
            $stolenCar->plate = $request->plate;
            $stolenCar->is_stolen = true;
            $stolenCar->description = $request->description;

            $stolenCar->save();

            return redirect()->back()->with('status', 'Car is declared as stolen');
        }

        $stolenCar->is_stolen = true;
        $stolenCar->description = $request->description;
        $stolenCar->save();

        return redirect()->back()->with('status', 'Car is declared as stolen');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function UnDeclareCarAsStolen(Request $request)
    {

        $this->restrictAccess();

        $stolenCar = StolenCar::where('plate', $request->plate)->first();

        if(!$stolenCar) {
            return redirect()->back()->with('error', 'Car with this plate is not in stolen cars list');
        }

        $stolenCar->is_stolen = false;
        $stolenCar->save();

        return redirect()->back()->with('status', 'Car is not stolen any more');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showFines()
    {

        $this->restrictAccess();

        return view(
            'police.fines',
            [
                'fineSearchRoute' => 'searchFines',
                'fines' => FineType::paginate(10)
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function searchFines(Request $request)
    {

        $this->restrictAccess();

        return view(
            'police.fines',
            [
                'fineSearchRoute' => 'searchFines',
                'fines' => FivemFineTypes::searchFines($request->search),
                'oldSearch' => $request->search
            ]
        );
    }

    /**
     * Show fine creation form
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showAddNewFine()
    {

        $this->restrictAccess();
        $this->restrictAccessForCaptain();

        return view(
            'police.addFine',
            [
                'action' => 'create',
                'submitRoute' => 'createNewFine'
            ]
        );
    }

    /**
     * Insert new fine to DB
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function createNewFine(Request $request)
    {

        $this->restrictAccess();
        $this->restrictAccessForCaptain();

        Validator::make(
            $request->all(),
            [
                'label' => 'required|string|max:250',
                'amount' => 'required|numeric',
                'category' => 'required|numeric|min:0|max:3'
            ]
        )->validate();
        $fine = new FineType();
        $fine->label = $request->label;
        $fine->amount = $request->amount;
        $fine->category = $request->category;
        $fine->min_jail = $request->minJail;
        $fine->max_jail = $request->maxJail;
        $fine->com_service_min = $request->comServiceMin;
        $fine->com_service_max = $request->comServiceMax;
        $fine->save();

        return redirect()->route('showFines')->with('success', 'New Fine Created');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function removeFine(Request $request)
    {

        $this->restrictAccess();
        $this->restrictAccessForCaptain();

        FineType::destroy($request->id);
        return redirect()->route('showFines')->with('success', ' Fine Removed');
    }

    /**
     * Show form where use can edit fine
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showEditFine($id)
    {

        $this->restrictAccess();
        $this->restrictAccessForCaptain();

        $fine = FineType::findOrFail($id);

        return view(
            'police.addFine',
            [
                'action' => 'update',
                'fine' => $fine,
                'submitRoute' => 'updateFine'
            ]
        );
    }

    /**
     * Update existing fine
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function updateFine(Request $request)
    {

        $this->restrictAccess();
        $this->restrictAccessForCaptain();

        Validator::make(
            $request->all(),
            [
                'label' => 'required|string|max:250',
                'amount' => 'required|numeric',
                'category' => 'required|numeric|min:0|max:3'
            ]
        )->validate();

        $fine = FineType::find($request->id);
        $fine->label = $request->label;
        $fine->amount = $request->amount;
        $fine->category = $request->category;
        $fine->min_jail = $request->minJail;
        $fine->max_jail = $request->maxJail;
        $fine->com_service_min = $request->comServiceMin;
        $fine->com_service_max = $request->comServiceMax;
        $fine->save();

        return redirect()->route('showFines')->with('success', 'Fine Updated');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showPoliceOfficersList()
    {

        $this->restrictAccess();

        return view(
            'police.allOfficers',
            [
                'users' => User::where('job', '=', 'police')
                    ->orWhere('job', '=', 'offpolice')
                    ->get(),
                'ranks' => JobGrades::where('job_name', '=', 'police')->get()
            ]
        );

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showApplicationsList()
    {

        $this->restrictAccess();
        $this->restrictAccessForCaptain();
        return view(
            'common.jobApplicationsList',
            [
                'applications' => JobApplication::with(['user', 'webUser'])->where('job', 'police')
                    ->where('isAccepted', false)
                    ->where('isRejected', false)
                    ->paginate(10),
                'singleApplicationRoute' => 'policeShowSingleApplication'
            ]
        );
    }

    /**
     * @param Request $request
     * @param $applicationId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showSingleApplication($applicationId)
    {

        $this->restrictAccess();
        $this->restrictAccessForCaptain();

        $application = JobApplication::with(['user', 'webUser'])->find($applicationId);;
        $answers = json_decode($application->answers);
        $user = $application->user()->first();

        return view(
            'common.jobApplication',
            [
                'application' => $application,
                'answers' => $answers,
                'user' => $user,
                'acceptRoute' => 'policeAcceptApplication',
                'rejectRoute' => 'policeRejectApplication'
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function acceptApplication(Request $request)
    {

        $this->restrictAccess();
        $this->restrictAccessForCaptain();

        $application = JobApplication::find($request->id);
        $application->isAccepted = true;
        $application->isRejected = false;
        $application->bossIdentifier = Auth::user()->id;
        $application->save();

        return redirect(route('policeShowApplicationsList'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function rejectApplication(Request $request)
    {

        $this->restrictAccess();
        $this->restrictAccessForCaptain();

        try {
            $application = JobApplication::find($request->id);
            $application->isAccepted = false;
            $application->isRejected = true;
            $application->bossIdentifier = Auth::user()->id;
            $application->save();
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }

        return redirect(route('policeShowApplicationsList'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showAcceptedJobApplications()
    {

        $this->restrictAccess();
        $this->restrictAccessForCaptain();
        return view(
            'common.jobApplicationsList',
            [
                'applications' => JobApplication::where('job', 'police')
                    ->where('isAccepted', true)
                    ->where('isRejected', false)
                    ->where('initiated', false)
                    ->paginate(10),
                'singleApplicationRoute' => 'policeShowSingleAcceptedApplication'
            ]
        );
    }

    /**
     * @param Request $request
     * @param $applicationId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showSingleAcceptedApplication($applicationId)
    {

        $this->restrictAccess();
        $this->restrictAccessForCaptain();

        $application = JobApplication::with(['user'])->find($applicationId);
        $answers = json_decode($application->answers);
        $user = $application->user()->first();

        return view(
            'common.jobApplication',
            [
                'application' => $application,
                'answers' => $answers,
                'user' => $user,
                'acceptRoute' => 'policeInitiateApplicantToWork',
                'acceptButtonName' => __('buttons.hire'),
                'rejectRoute' => 'policeRejectApplication'
            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function initiateApplicantToWork(Request $request)
    {

        $this->restrictAccess();
        $this->restrictAccessForCaptain();

        $application = JobApplication::find($request->id);
        $application->isAccepted = true;
        $application->isRejected = false;
        $application->initiated = true;
        $application->bossIdentifier = Auth::user()->id;
        $application->save();

        $policeOfficer = new PoliceOfficer();
        $policeOfficer->user_identifier = $application->user_identifier;
        $policeOfficer->is_hired = true;
        $policeOfficer->save();

        return redirect(route('policeShowAcceptedJobApplications'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function showEditCriminalRecord($id)
    {

        $this->restrictAccess();

        $criminalRecord = CriminalRecord::find($id);
        $bills = collect(json_decode($criminalRecord->fine));

        try {
            $minJail = FivemFineTypes::getMinJailTime($bills);
            $maxJail = FivemFineTypes::getMaxJailTime($bills);
        } catch (\Exception $exception) {
            $minJail = 0;
            $maxJail = 0;
        }

        return view(
            'police.criminalRecordForm',
            [
                'criminalIdentifier' => $criminalRecord->user_identifier,
                'officerIdentifier' => $criminalRecord->officer_id,
                'criminalRecord' => $criminalRecord,
                'bills' => $bills,
                'minJail' => $minJail,
                'maxJail' => $maxJail

            ]
        );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Exception
     */
    public function updateCriminalRecord(Request $request)
    {

        $this->restrictAccess();
        Validator::make(
            $request->all(),
            [
                'summary' => 'required|string|max:5000',
                'recordId' => 'required'
            ]
        )->validate();

        //dd($request);
        $criminalRecord = CriminalRecord::find($request->recordId);

        $criminalRecord->jail_time = $request->jailTime;
        $criminalRecord->warning_car = ($request->warningCar) ? true : false;;
        $criminalRecord->warning_driver_license = ($request->warningDriverLicense) ? true : false;;
        $criminalRecord->warning_gun_license = ($request->warningGunLicense) ? true : false;;
        $criminalRecord->summary = $request->summary;
        $criminalRecord->updated_at = new \DateTime();
        $criminalRecord->is_criminal_case = ($request->criminalCase) ? true: false;
        if ($request->criminalCase) {
            $criminalRecord->criminal_case_expires_at = (new \DateTime())->modify('+1 week');
        }
        $criminalRecord->save();

        return redirect(route('policeShowReport', ['id' => $request->recordId]));

    }

    /**
     * Control User job in Server and if not police or fib, then dont allow to continue
     */
    protected function restrictAccess()
    {

        $loggedInUserData = Auth::user()->fiveMUser()->first();

        $isAdmin = Auth::user()->hasRole('admin');
        if (!in_array($loggedInUserData->job, self::ALLOWED_JOBS) and !$isAdmin) {
            abort(401, 'This action is unauthorized.');
        }

        return;
    }

    /**
     * Control User job in Server and if not police or fib, then dont allow to continue
     */
    protected function restrictAccessForCaptain()
    {

        $loggedInUserData = Auth::user()->fiveMUser()->first();

        $isAdmin = Auth::user()->hasRole('admin');
        if (!in_array($loggedInUserData->job_grade, self::ALLOWED_JOB_GRADES) and !$isAdmin) {
            abort(401, 'This action is unauthorized.');
        }

        return;
    }
}
