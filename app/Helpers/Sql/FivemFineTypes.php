<?php
namespace App\Helpers\Sql;

use App\FineType;
use Illuminate\Support\Facades\DB;

class FivemFineTypes
{

    /**
     * @return \Illuminate\Support\Collection
     * @throws \Exception
     */
    public static function getFyneAllTypes()
    {

        try {
            return DB::table('fine_types')->get();
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * @param $keyword
     * @return \Illuminate\Support\Collection
     * @throws \Exception
     */
    public static function searchFines($keyword)
    {

        try {
            $keyword = strtolower($keyword);
            if(strpos($keyword, 'id:') !== false) {
                return DB::table('fine_types')
                    ->where('id', '=', explode(':',$keyword)[1])
                    ->get();
            }
            if(strpos($keyword, 'cat:') !== false) {
                return DB::table('fine_types')
                    ->where('category', '=', explode(':', $keyword)[1])
                    ->get();
            }
            $keyword = '%' . strtolower($keyword) . '%';
            return DB::table('fine_types')
                ->whereRaw('LOWER(`label`) like ?', $keyword)
                ->get();
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * @param $bills
     * @return float|int
     */
    public static function getMinJailTime($bills)
    {
        $collection = [];
        foreach ($bills as $bill){
            if($bill->target === 'society_police') {
                if (starts_with($bill->label, 'Trahv:')) {
                    $label = explode(':', $bill->label)[1];
                    $label = trim($label);
                } else {
                    $label = $bill->label;
                }
                $fine = FineType::where('label', $label)->first();
                $collection[] = $fine->min_jail;
            }
        }

        return array_sum($collection);
    }

    /**
     * @param $bills
     * @return float|int
     */
    public static function getMaxJailTime($bills)
    {
        $collection = [];
        foreach ($bills as $bill){
            if($bill->target === 'society_police') {
                if (starts_with($bill->label, 'Trahv:')) {
                    $label = explode(':', $bill->label)[1];
                    $label = trim($label);
                } else {
                    $label = $bill->label;
                }
                $fine = FineType::where('label', $label)->first();
                $collection[] = $fine->max_jail;
            }
        }

        return array_sum($collection);
    }
}
